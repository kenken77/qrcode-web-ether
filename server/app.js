require('dotenv').config();
var express = require("express");
var bodyParser = require('body-parser');
var request = require('request');
var _ = require('lodash');

const NODE_PORT = process.env.PORT;

var app = express();

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(express.static(__dirname + "/../client/"));

var Client = require('coinbase').Client;
var client = new Client({'apiKey': process.env.COINBASE_API_KEY, 'apiSecret': process.env.COINBASE_SECRET});

app.get("/balance/:crypto_address", function (req, res) {
    let accAddress = req.params.crypto_address;
    let arraryOfCurrentAddress = accAddress.split(':');
    client.getAccounts({}, function(err, accounts) {
        if(err) console.log(err);
        accounts.forEach(function(acct) {
          if(arraryOfCurrentAddress[0] === 'litecoin' && acct.name === 'LTC Wallet'){
            res.status(200).json({balance: acct.balance.amount});
          }else if(arraryOfCurrentAddress[0] === 'ethereum' && acct.name === 'ETH Wallet'){
            res.status(200).json({balance: acct.balance.amount});
          }
        }); 
    });
});  

app.listen(NODE_PORT, function () {
    console.log("Server running at http://localhost:" + NODE_PORT);
});
