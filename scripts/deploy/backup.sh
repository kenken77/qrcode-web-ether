#!/bin/bash
QRCODE_HOME=$HOME/qr-deploy

if [ -d "$QRCODE_HOME" ]; then
   zip -r $HOME/backup/$(date +"%m-%d-%Y").zip $QRCODE_HOME/
fi
rm -rf $QRCODE_HOME/*
rm -rf $QRCODE_HOME/.git