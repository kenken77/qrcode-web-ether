(function () {
    angular
        .module("demoApp")
        .controller("DemoCtrl", DemoCtrl, '$http', '$scope')

    function DemoCtrl($http, $scope) {
        var self = this;
        self.toEtherAddress = "";
        self.balance = 0;
        self.transerAmount = 0;
        $scope.string = '';

        self.start = function() {
            self.cameraRequested = true;
        }

        self.processEtherAddressfromQR = function (ethAddress) {
            self.etherAddress = ethAddress;
            self.cameraRequested = false;
            console.log(self.etherAddress.split("ethereum:"));
            $scope.string = self.etherAddress;
            
            $http({
                method: 'GET',
                url: `/balance/${self.etherAddress}`,
            }).then(function successCallback(response) {
                console.log(response);
                self.account = response.data;
            }, function errorCallback(response) {
                 console.log(response);
            });
        }
    }

})();